﻿using Cinemachine;
using Core.entity;
using Core.events;
using Core.spectator;
using Player;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.weapon;

namespace ZombieProject.game.specialitem.drone
{
    public class Drone : Entity, ISpectator
    {
        [SerializeField] private WeaponSo weaponSo;
        [SerializeField] private CinemachineVirtualCamera virtualCamera;

        private Weapon _weapon;
        private Core.entity.Player _player;

        private bool _isActive;

        public bool IsActive
        {
            get => _isActive;
            private set
            {
                _isActive = value;
                if (value) GameEventInstance.Instance.SpecialItemEvent.OnDroneActiveEvent(this);
                else GameEventInstance.Instance.SpecialItemEvent.OnDroneDeActiveEvent(this);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _player = FindObjectOfType<Core.entity.Player>();
            _weapon = WeaponInstance.GetWeapon(weaponSo.weaponType);
            _weapon.Init(weaponSo, this, GetCamera().transform);
        }

        private void OnEnable()
        {
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent += OnPlayerSpectateEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent -= OnPlayerSpectateEvent;
        }

        protected override void Update()
        {
            base.Update();

            //TODO: Fixes nested statement
            if (!IsActive) return;
            if (Input.GetMouseButton(0))
            {
                if (_weapon.GetAmmo() == 0)
                {
                    _weapon.Reload();
                    return;
                }

                _weapon.Shoot();
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                PlayerSpectator.Instance.SetCamera(_player);
            }
        }

        public CinemachineVirtualCamera GetCamera()
        {
            return virtualCamera;
        }

        private void OnPlayerSpectateEvent(ISpectator spectator)
        {
            IsActive = spectator is Drone;
        }

        public Weapon GetWeapon()
        {
            return _weapon;
        }
    }
}