﻿using System.Collections.Generic;
using Core.entity;
using Core.events;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game.checkpoint;
using ZombieProject.game.enemy;
using ZombieProject.ui.healthbar;

namespace ZombieProject.game.wave
{
    public class WaveSpawner : MonoBehaviour, IWaveSpawner
    {
        private List<Monster> _enemies;
        private IWaveManager _waveManager;

        private void Awake()
        {
            _enemies = new List<Monster>();
        }

        private void OnEnable()
        {
            EventInstance.Instance.EntityEvent.EntityDeathEvent += OnEntityDeathEvent;
            GameEventInstance.Instance.GameEvent.EnemyEnterDefencePointEvent += OnEnemyEnterDefencePointEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.EntityEvent.EntityDeathEvent -= OnEntityDeathEvent;
            GameEventInstance.Instance.GameEvent.EnemyEnterDefencePointEvent -= OnEnemyEnterDefencePointEvent;
        }

        public void Init(IWaveManager waveManager)
        {
            _waveManager = waveManager;
        }

        public List<Monster> GetEnemies()
        {
            return _enemies;
        }

        public void SpawnEnemy(EntityType entityType, CheckPoint checkPoint)
        {
            var enemy = (Monster) EntityInstance.Instance.SpawnEntity(entityType, checkPoint.GetLocation());
            enemy.SetLocation(checkPoint.GetLocation());
            enemy.gameObject.AddComponent<EnemyAI>().SetCheckPoint(checkPoint);
            var offset = new Vector3(0, enemy.GetEyeTransform().position.y + 1f);
            HealthBarInstance.SetHealthBar(enemy, HealthBarType.DEFAULT, offset);
            _enemies.Add(enemy);
        }

        public bool IsWaveEnemy(Monster enemy)
        {
            return _enemies.Contains(enemy);
        }

        private void OnEntityDeathEvent(LivingEntity livingEntity)
        {
            if (!(livingEntity is Monster monster)) return;
            if (!IsWaveEnemy(monster)) return;
            _enemies.Remove(monster);
            GameEventInstance.Instance.GameEvent.OnEnemyKilledEvent(monster);
            CheckWave();
        }

        private void OnEnemyEnterDefencePointEvent(IWaveManager arg1, Monster monster)
        {
            if (!IsWaveEnemy(monster)) return;
            _enemies.Remove(monster);
            CheckWave();
        }

        private void CheckWave()
        {
            if (_enemies.Count < 1) GameEventInstance.Instance.GameEvent.OnWaveClearedEvent(_waveManager);
        }
    }
}