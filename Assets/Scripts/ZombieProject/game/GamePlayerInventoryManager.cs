﻿using Player.data;
using UnityEngine;
using ZombieProject.weapon;
using ZombieProject.weapon.factory;

namespace ZombieProject.game
{
    public class GamePlayerInventoryManager : MonoBehaviour
    {
        [SerializeField] private WeaponSo defaultWeapon;
        [SerializeField] private UserSaveSO userSaveSo;
        [SerializeField] private Core.entity.Player player;

        private IWeaponCommandItemFactory _weaponCommandItemFactory;

        private void Awake()
        {
            _weaponCommandItemFactory = GetComponent<IWeaponCommandItemFactory>();
        }

        private void Start()
        {
            SetupPlayerWeapon();
        }

        private void SetupPlayerWeapon()
        {
            if (userSaveSo.equippedItem.Count > 0)
            {
                foreach (var weapon in userSaveSo.equippedItem)
                    AddWeapon(weapon);
                return;
            }

            AddWeapon(defaultWeapon);
        }

        private void AddWeapon(WeaponSo weapon)
        {
            var weaponCommandItem = _weaponCommandItemFactory.Create(weapon);
            weaponCommandItem.GetWeapon().Init(weapon, player, player.GetEyeTransform());
            player.GetInventory().AddItem(weaponCommandItem);
        }
    }
}