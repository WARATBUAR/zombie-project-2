﻿using ZombieProject.game;
using ZombieProject.tower.inventory.item;

namespace ZombieProject.tower.builder
{
    public interface ITowerBuilderUpgradeResponse
    {
        void OnUpgrade(WaveManager waveManager, ITowerBuilder towerBuilder,
            TowerCommandItem towerCommandItem);
    }
}