﻿using Core.entity;
using UnityEngine;
using Utils;
using ZombieProject.weapon;

namespace ZombieProject.tower.weapon
{
    public class TowerWeapon : MonoBehaviour, ITowerWeapon
    {
        [SerializeField] private WeaponSo weaponSo;
        [SerializeField] private Transform headTransform;
        [SerializeField] private Transform shootTransform;
        [SerializeField] private float lookTargetSpeed;

        private Weapon _weapon;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(shootTransform.transform.position, _weapon.GetWeaponSo().range);
        }

        public Transform GetShootPoint()
        {
            return shootTransform;
        }

        public void Init(Tower tower)
        {
            var weapon = WeaponInstance.GetWeapon(weaponSo.weaponType);
            weapon.Init(weaponSo, tower, shootTransform);
            _weapon = weapon;
        }

        public IWeapon GetWeapon()
        {
            return _weapon;
        }

        public void Attack(LivingEntity target)
        {
            //Simple Tower AI Logic
            var dirToTarget = (target.GetEyeTransform().position - shootTransform.transform.position).normalized;
            var lookRotation = Quaternion.LookRotation(dirToTarget);
            headTransform.rotation =
                Quaternion.Lerp(headTransform.rotation, lookRotation, Time.deltaTime * lookTargetSpeed);

            var fovToTarget = GameMath.GetTargetFOV(shootTransform, target.transform);
            if (fovToTarget > 10f) return;

            if (_weapon.IsReloading) return;
            if (!_weapon.HasAmmo())
            {
                _weapon.Reload();
                return;
            }

            _weapon.Shoot();
        }
    }
}