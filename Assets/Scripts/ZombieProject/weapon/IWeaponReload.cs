﻿namespace ZombieProject.weapon
{
    public interface IWeaponReload
    {
        void Reload(IWeapon weapon);
    }
}