﻿using System;
using UnityEngine;

namespace ZombieProject.weapon.ui
{
    [CreateAssetMenu(menuName = "Item/Hand Item Model Pack")]
    public class HandItemPack : ScriptableObject
    {
        [SerializeField] private GameObject defaultModel;
        [SerializeField] private WeaponData[] itemIconData;

        public GameObject GetWeaponModel(EnumValue enumValue)
        {
            foreach (var data in itemIconData)
                if (data.enumValue.Equals(enumValue))
                    return data.model;

            return defaultModel;
        }

        [Serializable]
        public struct WeaponData
        {
            public EnumValue enumValue;
            public GameObject model;
        }
    }
}