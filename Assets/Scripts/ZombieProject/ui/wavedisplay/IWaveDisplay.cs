﻿namespace ZombieProject.ui.wavedisplay
{
    public interface IWaveDisplay
    {
        void SetWave(int current, int maxWave);
    }
}