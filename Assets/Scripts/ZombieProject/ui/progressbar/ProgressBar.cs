﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ZombieProject.ui.progressbar
{
    public class ProgressBar : MonoBehaviour, IProgressBar
    {
        [SerializeField] private TextMeshProUGUI progressBarText;
        [SerializeField] private Image progressBarImage;

        public void SetProgress(float current, float time)
        {
            progressBarText.text = $"{current:F1} / {time}";
            var progressBar = current / time;
            progressBarImage.fillAmount = progressBar;
        }
    }
}