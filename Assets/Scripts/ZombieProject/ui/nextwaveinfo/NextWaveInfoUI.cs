﻿using Core.ui;
using ZombieProject.events;
using ZombieProject.game.wave;

namespace ZombieProject.ui.nextwaveinfo
{
    public class NextWaveInfoUI : BaseUI
    {
        private INextWaveInfo _nextWaveInfo;

        public override void Awake()
        {
            base.Awake();
            _nextWaveInfo = GetComponent<INextWaveInfo>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameEventInstance.Instance.GameEvent.WaveStartEvent += OnWaveStartEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameEventInstance.Instance.GameEvent.WaveStartEvent -= OnWaveStartEvent;
        }

        private void OnWaveStartEvent(IWaveManager waveManager)
        {
            if (waveManager.IsFinalWave())
            {
                _nextWaveInfo.ClearInfo();
                return;
            }

            _nextWaveInfo.SetWaveInfo(waveManager.GetNextWaveSo());
        }
    }
}