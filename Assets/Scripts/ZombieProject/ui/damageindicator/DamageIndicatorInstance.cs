﻿using System;
using Singleton;
using UnityEngine;
using ZombieProject.pool;
using Debug = UnityEngine.Debug;

namespace ZombieProject.ui.damageindicator
{
    public class DamageIndicatorInstance : ResourceSingleton<DamageIndicatorInstance>
    {
        [SerializeField] private int prewarm = 2;
        [SerializeField] private DamageIndicator damageIndicatorPrefab;
        [SerializeField] private DamageIndicatorPool damageIndicatorPool;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(damageIndicatorPool != null, "DamageIndicatorPool!=null");

            damageIndicatorPool.Init(damageIndicatorPrefab);
            damageIndicatorPool.Prewarm(prewarm);
        }

        private void OnHitFloor(DamageIndicator damageIndicator)
        {
            damageIndicatorPool.Return(damageIndicator);
        }

        public static DamageIndicator SetDamage(Vector3 location, float damage, Color color)
        {
            var damageIndicator = Instance.damageIndicatorPool.Request();
            damageIndicator.Init(damage,color);
            damageIndicator.transform.position = location;
            damageIndicator.OnHitFloor = Instance.OnHitFloor;
            return damageIndicator;
        }
    }
}