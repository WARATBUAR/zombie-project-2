﻿namespace Core.projectile
{
    public enum ProjectileType
    {
        BULLET_MM,
        BULLET_BMG,
        EXPLOSIVE,
        RAY,
    }
}