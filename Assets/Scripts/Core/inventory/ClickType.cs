﻿namespace Core.inventory
{
    public enum ClickType
    {
        NONE,
        LEFT,
        RIGHT,
        MIDDLE
    }
}