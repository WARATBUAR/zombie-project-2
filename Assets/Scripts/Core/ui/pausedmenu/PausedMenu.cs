﻿using Core.events;
using Core.ui.gamebutton;
using Core.ui.mainmenu.settings;
using UnityEngine;
using Utils.scene;

namespace Core.ui.pausedmenu
{
    public class PausedMenu : SettingsMenu
    {
        [Header("PAUSED MENU")] [SerializeField]
        private GameButton restartButton;

        [SerializeField] private GameButton quitButton;
        [SerializeField] private Transform group;

        protected override void Awake()
        {
            base.Awake();
            quitButton.SetClickAction(() => { GameSceneManager.LoadScene(GameSceneType.MAP); });
            restartButton.SetClickAction(GameSceneManager.RestartCurrentScene);
            Hide();
        }

        public override void Show()
        {
            group.gameObject.SetActive(true);
            EventInstance.Instance.UIEvent.OnToggleMenuEvent(true);
        }

        public override void Hide()
        {
            group.gameObject.SetActive(false);
            EventInstance.Instance.UIEvent.OnToggleMenuEvent(false);
        }
    }
}