﻿using Core.events;
using Core.inventory;
using ScriptableObjects.Inventory;
using UnityEngine;

namespace Core.ui.handitem
{
    public class HandItem : MonoBehaviour
    {
        [SerializeField] private ItemIconPack itemIconPack;
        [SerializeField] private SpriteRenderer handItemRenderer;
        [SerializeField] private Sprite emptySprite;

        private void OnEnable()
        {
            EventInstance.Instance.InventoryEvent.PlayerItemHeldEvent += OnPlayerItemHeldEvent;
            EventInstance.Instance.InventoryEvent.PlayerInventoryUpdateEvent += OnPlayerInventoryUpdateEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.InventoryEvent.PlayerItemHeldEvent -= OnPlayerItemHeldEvent;
            EventInstance.Instance.InventoryEvent.PlayerInventoryUpdateEvent -= OnPlayerInventoryUpdateEvent;
        }

        private void OnPlayerItemHeldEvent(entity.Player player, int arg2, int arg3)
        {
            var handItem = player.GetInventory().GetItemInHand();
            handItemRenderer.sprite = handItem == null ? emptySprite : itemIconPack.GetItemIcon(handItem.GetType());
        }

        private void OnPlayerInventoryUpdateEvent(IPlayerInventory playerInventory)
        {
            var handItem = playerInventory.GetItemInHand();
            handItemRenderer.sprite = handItem == null ? emptySprite : itemIconPack.GetItemIcon(handItem.GetType());
        }
    }
}