﻿using UnityEngine;
using UnityEngine.UI;
using Utils.scene;

namespace Core.ui.splashscreen
{
    public class SplashScreen : MonoBehaviour
    {
        [SerializeField] private Button skip;

        private void Awake()
        {
            skip.onClick.AddListener(() => { GameSceneManager.LoadScene(GameSceneType.MAINMENU); });
        }
    }
}