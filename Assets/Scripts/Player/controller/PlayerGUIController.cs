﻿
using Core.events;
using Core.ui.pausedmenu;
using UnityEngine;
using ZombieProject.events;

namespace Player.controller
{
    public class PlayerGUIController : MonoBehaviour
    {
        [SerializeField] private Core.entity.Player player;
        [SerializeField] private PausedMenu pausedMenu;

        private bool _isShowMenu;

        private void OnEnable()
        {
            EventInstance.Instance.InputEvent.InventoryEvent += OnInventoryEvent;
            EventInstance.Instance.InputEvent.ToggleMenuEvent += OnToggleMenuEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent += OnToggleMenuEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent += OnToggleMenuEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.InputEvent.InventoryEvent -= OnInventoryEvent;
            EventInstance.Instance.InputEvent.ToggleMenuEvent -= OnToggleMenuEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent -= OnToggleMenuEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
        }

        private bool _isGameOver;

        private void OnGameOverEvent(bool win)
        {
            _isGameOver = true;
        }

        private void OnInventoryEvent()
        {
            if (_isShowMenu || _isGameOver) return;
            var openInventory = player.GetOpenInventory();
            if (openInventory != null)
            {
                player.CloseInventory();
                return;
            }

            player.OpenInventory(player.GetInventory());
        }

        private void OnToggleMenuEvent()
        {
            if (_isGameOver) return;
            var openInventory = player.GetOpenInventory();
            if (openInventory != null)
            {
                player.CloseInventory();
                return;
            }

            _isShowMenu = !_isShowMenu;
            if (_isShowMenu) pausedMenu.Show();
            else pausedMenu.Hide();
        }

        private void OnToggleMenuEvent(bool show)
        {
            _isShowMenu = show;
        }
    }
}