﻿using UnityEngine;

namespace Player.data
{
    [CreateAssetMenu(menuName = "Player Option", order = 0)]
    public class UserOptionSO : ScriptableObject
    {
        public bool isMusicOn;
        public bool isSoundOn;
        public float sensitivity;
    }
}