﻿using UnityEngine;

namespace Utils
{
    public class LookTowardCameraIgnoreY : MonoBehaviour
    {
        private Transform _camTransform;

        private void Awake()
        {
            _camTransform = Camera.main.transform;
        }

        private void FixedUpdate()
        {
            var lookDir = _camTransform.forward;
            lookDir.y = 0f;
            transform.rotation = Quaternion.LookRotation(lookDir);
        }
    }
}