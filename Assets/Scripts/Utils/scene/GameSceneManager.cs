﻿using UnityEngine.SceneManagement;

namespace Utils.scene
{
    public static class GameSceneManager
    {
        public static void RestartCurrentScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public static void LoadScene(GameSceneType gameSceneType)
        {
            var sceneIndex = (int) gameSceneType;
            SceneManager.LoadScene(sceneIndex);
        }
    }
}