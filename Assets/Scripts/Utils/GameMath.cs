﻿using UnityEngine;

namespace Utils
{
    public static class GameMath
    {
        public static float GetTargetFOV(Transform origin, Transform target)
        {
            //Θ = Cos-1(V.W / |V| |W|)
            var targetPos = target.position;
            targetPos.y = 0f;
            var originPos = origin.position;
            originPos.y = 0f;

            var dirToTarget = (targetPos - originPos).normalized;
            var dot = Vector3.Dot(origin.forward, dirToTarget);
            var angle = Mathf.Acos(dot);
            var degree = angle * Mathf.Rad2Deg;

            return degree;
        }
    }
}