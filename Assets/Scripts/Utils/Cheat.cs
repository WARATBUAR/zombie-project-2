﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ZombieProject.game;

namespace Utils
{
    public class Cheat : MonoBehaviour
    {
        [SerializeField] private GameDebug gameDebug;
        
        private readonly Dictionary<string, Action> _cheats = new Dictionary<string, Action>();
        
        private void Awake()
        {
            gameDebug.OnRunCommand += OnRunCommandEvent;
            SetupCheats();
        }

        private void SetupCheats()
        {
            _cheats.Add("help", () =>
            {
                gameDebug.AddLog("----[ Commands ]----");
                gameDebug.AddLog("/god");
                gameDebug.AddLog("/money");
            });
            
            _cheats.Add("money", () =>
            {
                FindObjectOfType<WaveManager>().Coin = 99999;
                gameDebug.AddLog("money: Unlimited Money: true");
            });
            
            _cheats.Add("god", () =>
            {
                FindObjectOfType<WaveManager>().HealthPoint = 99999;
                gameDebug.AddLog("god: God Mode: true");
            });
        }
        private void OnRunCommandEvent(string cmd)
        {
            if(_cheats.ContainsKey(cmd))
                _cheats[cmd].Invoke();
            
            Debug.Log(cmd);
        }
    }
}