﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Image))]
    public class SpriteAnimation : MonoBehaviour
    {
        [SerializeField] private Sprite[] iSprites;
        [SerializeField] private float delay;
        private Image _image;
        private int _currentSpriteIndex;
        private float _currentTimer;

        private void Awake()
        {
            if (delay < 0.05)
                delay = 0.05f;

            _image = GetComponent<Image>();
        }

        private Sprite GetImage()
        {
            return iSprites[_currentSpriteIndex++ % iSprites.Length];
        }

        private void FixedUpdate()
        {
            if (_currentTimer > 0)
            {
                _currentTimer -= Time.fixedTime;
                return;
            }

            _currentTimer = delay;
            _image.sprite = GetImage();
        }
    }
}